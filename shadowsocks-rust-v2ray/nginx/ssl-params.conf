# Paths to certs.
ssl_certificate     /certs/link-cert.fullchain;
ssl_certificate_key /certs/link-cert.key;

# Allow only the newest TLS protocol published in August 2018.
# @see https://caniuse.com/tls1-3
ssl_protocols TLSv1.3;

# Enables TLS 1.3 early data.
ssl_early_data on;

# Enable ssl cache and keepalive connections to greatly minimize the number of cpu-expensive "SSL handshake" operations.
# @see https://nginx.org/en/docs/http/configuring_https_servers.html#optimization
# @see https://serverfault.com/a/695260
ssl_session_cache   shared:SSL:10m;
ssl_session_timeout 10m;
keepalive_timeout   75s;

# nginx will use "ssl_ciphers" setting only if this setting is enabled.
ssl_prefer_server_ciphers on;

# Use a smaller ssl_buffer_size to minimize TTFB (Time To First Byte).
# @see https://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_buffer_size
# @see https://haydenjames.io/nginx-tuning-tips-tls-ssl-https-ttfb-latency
ssl_buffer_size 8k;

# Enabling OCSP stapling allows the Nginx to bear the resource cost involved in providing OCSP responses
# by appending (“stapling”) a time-stamped OCSP response signed by the CA to the initial TLS handshake,
# eliminating the need for clients to contact the CA.
ssl_stapling on;
ssl_stapling_verify on;
ssl_trusted_certificate /certs/link-cert.fullchain;
