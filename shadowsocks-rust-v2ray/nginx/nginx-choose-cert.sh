#!/bin/sh

set -e

# Define paths:
CERTS_PATH="/certs"
LINK_CERT="${CERTS_PATH}/link-cert.fullchain"
LINK_KEY="${CERTS_PATH}/link-cert.key"
SELF_CERT="${CERTS_PATH}/${SERVER_NAME}.self-signed.cert"
SELF_KEY="${CERTS_PATH}/${SERVER_NAME}.self-signed.key"
REAL_CERT="${CERTS_PATH}/${SERVER_NAME}.fullchain"
REAL_KEY="${CERTS_PATH}/${SERVER_NAME}.key"

# Generate a fallback self-signed cert:
if [ ! -f "${SELF_KEY}" ]; then
    openssl req -x509 -newkey rsa:4096 -keyout "${SELF_KEY}" -out "${SELF_CERT}" -nodes -days 36500 -subj "/CN=${SERVER_NAME}"
fi

# Remove existing links:
rm -f "${LINK_CERT}"
rm -f "${LINK_KEY}"

# Use the real cert if it exists or the self-signed one if not:
if [ -f "${REAL_CERT}" ]; then
  ln -s "${REAL_CERT}"  "${LINK_CERT}"
  ln -s "${REAL_KEY}"   "${LINK_KEY}"
else
  ln -s "${SELF_CERT}"  "${LINK_CERT}"
  ln -s "${SELF_KEY}"   "${LINK_KEY}"
fi
