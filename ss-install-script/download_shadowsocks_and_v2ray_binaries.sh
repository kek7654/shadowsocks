#!/bin/bash
# ------------------------------------------------------------------------------
# shadowsocks-rust and v2ray-plugin binaries installation/upgrade script.
# ------------------------------------------------------------------------------
# @see https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
git_latest_release() {
    wget -qO- "https://api.github.com/repos/$1/releases/latest" |   # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

# ------------------------------------------------------------------------------
INSTALL_DIR="/usr/local/bin"
SS_VERSION=$(git_latest_release "shadowsocks/shadowsocks-rust")
V2RAY_VERSION=$(git_latest_release "shadowsocks/v2ray-plugin")
# ------------------------------------------------------------------------------

# Install shadowsocks:
wget https://github.com/shadowsocks/shadowsocks-rust/releases/download/"${SS_VERSION}"/shadowsocks-"${SS_VERSION}".x86_64-unknown-linux-gnu.tar.xz && \
    tar -xf *.tar.xz && \
    rm *.tar.xz && \
    mv -t "$INSTALL_DIR"/ sslocal ssmanager ssserver ssservice ssurl && \
    chmod +x "$INSTALL_DIR"/sslocal "$INSTALL_DIR"/ssmanager "$INSTALL_DIR"/ssserver "$INSTALL_DIR"/ssservice "$INSTALL_DIR"/ssurl

# Install v2ray-plugin:
wget https://github.com/shadowsocks/v2ray-plugin/releases/download/"${V2RAY_VERSION}"/v2ray-plugin-linux-amd64-"${V2RAY_VERSION}".tar.gz && \
    tar -xf *.tar.gz && \
    rm *.tar.gz && \
    mv v2ray* "$INSTALL_DIR"/v2ray-plugin && \
    chmod +x "$INSTALL_DIR"/v2ray-plugin

# Show installed versions:
echo "The following packages have been installed into the \"$INSTALL_DIR\":"
"$INSTALL_DIR"/sslocal --version
"$INSTALL_DIR"/v2ray-plugin --version
