#!/bin/bash
# ------------------------------------------------------------------------------
# Xray binary and systemd service installation/upgrade script.
# ----------
# Usage: sudo xray_download.sh [specific_xray_version or the latest one will be used]
# ------------------------------------------------------------------------------
main() {
    local XRAY_REPOSITORY="XTLS/Xray-core"
    local XRAY_VERSION="${1}"
    if [ -z "${XRAY_VERSION}" ]; then
        XRAY_VERSION=$(git_latest_release "${XRAY_REPOSITORY}")
    fi;

    # Install xray:
    (XRAY_DOWN_TEMP_DIR=$(mktemp -d) && cd "${XRAY_DOWN_TEMP_DIR}" && \
        wget "https://github.com/${XRAY_REPOSITORY}/releases/download/${XRAY_VERSION}/Xray-linux-64.zip" && \
        unzip -o Xray-linux-64.zip "xray" -d "/usr/local/bin/" && \
        unzip -o Xray-linux-64.zip "geoip.dat" -d "/usr/local/share/xray/" && \
        unzip -o Xray-linux-64.zip "geosite.dat" -d "/usr/local/share/xray/" && \
        chmod +x /usr/local/bin/xray && \
        mkdir -p /usr/local/etc/xray/ && \
        rm -r "${XRAY_DOWN_TEMP_DIR}")

    # Copy systemd service:
    copy_systemd_service
    enable_systemd_service
    
    # Show installed versions:
    echo -e "\n$(/usr/local/bin/xray --version)"
}

copy_systemd_service() {
    cat > /etc/systemd/system/xray.service << EOF
[Unit]
Description=Xray Service
Documentation=https://github.com/XTLS
After=network.target nss-lookup.target

[Service]
Type=simple
DynamicUser=yes
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
NoNewPrivileges=true
Restart=on-failure
RestartPreventExitStatus=23
LimitNPROC=10000
LimitNOFILE=1000000

ExecStart=/usr/local/bin/xray -config /usr/local/etc/xray/config.json

[Install]
WantedBy=multi-user.target
EOF
}

enable_systemd_service() {
    systemctl enable xray.service
}

# @see https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
git_latest_release() {
    wget -qO- "https://api.github.com/repos/$1/releases/latest" |   # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

#-------------------------------------------------------------------------------
# @see https://unix.stackexchange.com/a/449508/
main "$@"; exit
