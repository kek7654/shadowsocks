#!/bin/bash
# ------------------------------------------------------------------------------
# Usage: sudo xray_restart_with_config.sh my_config (will use /usr/local/etc/xray/my_config.json)
# ------------------------------------------------------------------------------

SERVICE_NAME="xray.service"
CONFIG_PATH="/usr/local/etc/xray"
SYMLINK="${CONFIG_PATH}/config.json"
CONFIG_FILENAME="${CONFIG_PATH}/${1}.json"

if [[ ! -f "${CONFIG_FILENAME}" ]] ; then
    echo "Wrong config name: \"${1}\"! Existed configs:"
    ls -l "${CONFIG_PATH}"
    exit 1
fi

systemctl stop ${SERVICE_NAME} && \
    ln -sf "${CONFIG_FILENAME}" "${SYMLINK}"  && \
    echo -e "new config: $(readlink -f "${SYMLINK}")" && \
    systemctl daemon-reload && \
    systemctl restart ${SERVICE_NAME} && \
    sleep 1 && \
    systemctl status ${SERVICE_NAME} -l --no-pager
