# Установка shadowsocks-клиента + v2ray на Linux (Ubuntu)

Подразумевается, что сервер у вас уже настроен. В этом гайде описывается настройка **клиента**, который будет подключаться к этому самому серверу.

## В чем отличие от [этого гайда](https://f-gzhechko.medium.com/shadowsocks-go-how-to-step-by-step-guide-beda4352b52f):
TL;DR - их нет.

В статье на медиуме автор занимается хуйней:

- разместил команды в медиуме, который их пидорасит
- использует `go-shadowsocks2`, когда разработчики пилят как основную версию `shadowsocks-rust`
- устанавливает только голый `shadowsocks`, а `v2ray` предлагает устанавливать через отдельный не менее ебанутый гайд. В итоге в системе 2 демона, на самом деле, достаточно и одного.
- задает настройки подключения прямо в `systemd`-юните.

В этом гайде:

- замечательный [скрипт](https://bitbucket.org/kek7654/shadowsocks/src/master/ss-install-script/download_shadowsocks_and_v2ray_binaries.sh), автоматически скачивающий свежие бинари `shadowsocks-rust` и `v2ray` с официальных гитхабов.
- максимально упрощенный `systemd`-юнит, в одну команду запускается `shadowsocks` и `v2ray`. Также, благодаря помощи анонов, были убраны ненужные опции из юнита.
- настройки подключения размещены в `json`-файлике. Не нужно редактировать `systemd`-юнит в случае их изменения.
- команды, которые можно нормально копировать.

## Установка:
1. Устанавливаем зависимости:
```shell
sudo apt-get update && sudo apt-get install -y wget xz-utils git
```

2. Скачиваем этот репозиторий:
```shell
git clone https://bitbucket.org/kek7654/shadowsocks.git && cd shadowsocks/ss-install-script
```

3. Скачиваем бинарники `shadowsocks` и `v2ray` с официальных гитхабов с помощью скрипта. По-умолчанию, он сохранит их в `/usr/bin`. По-умолчанию, скачиваются последние версии.
```shell
sudo ./download_shadowsocks_and_v2ray_binaries.sh
```

4. Заполняем настройки подключения в файле `shadowsocks-client-config.json`:
```shell
sudo cp shadowsocks-client-config.json /etc && sudo nano /etc/shadowsocks-client-config.json
```

5. Создаем `systemd`-юнит (скрипт автоматического запуска/остановки демона). Задаем путь к бинарю `sslocal` и путь к конфигурационному `json`-файлу. Пути должны быть абсолютными.
```shell
sudo cp sslocal.service /etc/systemd/system/ && sudo nano /etc/systemd/system/sslocal.service
```

6. Запускаем демона:
```shell
sudo systemctl daemon-reload && sudo systemctl restart sslocal.service && sudo systemctl enable sslocal.service && sudo systemctl status sslocal.service
```


Если все прошло хорошо, статус демона должен выглядеть как-то так:
```
● sslocal.service - Daemon to start Shadowsocks Client
     Loaded: loaded (/etc/systemd/system/sslocal.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-04-27 23:13:58 MSK; 2h 1min ago
   Main PID: 1631340 (sslocal)
      Tasks: 23 (limit: 18810)
     Memory: 28.7M
     CGroup: /system.slice/sslocal.service
             ├─1631340 /usr/bin/sslocal -c /etc/shadowsocks-client-config.json
             └─1631352 /usr/bin/v2ray-plugin
```

Теперь в системе на адресе `local_address` и порту `local_port` будет висеть `SOCKS5`-прокси, к которой можно подключиться с любых поддерживающих это приложений.
