# Учимся работать с SSH-ключами
Настало время сделать SSH-ключи своими верными друзьями, а не источником жопной боли. Пора подключаться к серверу, просто вводя `ssh moy_server` вместо пердолинга с паролями. А также иметь возможность использовать много других крутых утилит, которые умеют работать с удаленным сервером, если настроено ssh-подключение (scp, rsync). Здесь ты можешь этому научиться.

## Prerequisites
1. Данный гайд, как и все остальные, рассчитан на *более-менее уверенных пользователей* Linux.
2. Все описанные шаги протестированы в Ubuntu 20.04. Можно с таким же успехом проделать в WSL на Windows 10.

## Вводный ликбез
1. SSH-ключи хранятся в директории `~/.ssh/`.
2. Права на приватный ключ: `600`, на публичный (.pub): `644`. При генерации пары ключей ssh-keygen'ом, нужные права на них сразу проставляются автоматически. Если же у тебя **уже** есть пара ключей, откуда бы ты ее ни взял, то тебе надо копирнуть их в данную директорию вручную и самостоятельно проставить права с помощью `chmod`.
3. Новый SSH-ключ генерируется командой `ssh-keygen -t rsa -b 4096`. Если будешь тупо тыкать enter, то ключ сохранится под именем `~/.ssh/id_rsa` и `~/.ssh/id_rsa.pub`. Но я настоятельно рекомендую задавать осмысленное имя при генерации ключа, чтобы было понятно, от чего этот ключ. Сохраняй их под именами вида `~/.ssh/id_rsa_moy_ohuenniy_kluch`.
4. Сразу, не отходя от кассы, привыкай настраивать `~/.ssh/config`. Иначе потом ОЧЕ сильно охуеешь от весьма нетривиальных багов, когда все вроде бы правильно настроено, но сервер шлет нахуй. Особенно когда у тебя много ключей. Про настройку `~/.ssh/config` будет дальше отдельный пункт.
5. Парольные фразы (passphrase). Нахуй их. Создают ОЧЕНЬ много дополнительного геморроя. Я скозал, что SSH-ключи достаточно безопасны сами по себе, а если ты просрешь ключи, то тебе один хуй ничего не поможет, так что нахуй их. Всегда оставляй пустыми.
6. Чтобы к серверу было возможно подключиться по ssh, публичный ключ должен быть добавлен в `~/.ssh/authorized_keys` на сервере.

## ~/.ssh/config
Данный файл выполняет крайне важную функцию - определяет, к каким айпишникам/доменам применять каждый конкретный ключ.
А то, если ты не задашь эти правила, линух будет при каждом ssh-подключении к любому хосту **перебирать все ключи**. Тупо по очереди.
А сервера обычно имеют весьма мелкий лимит на число попыток подключения и с высокой вероятностью пошлют тебя нахуй уже после 3-й или 5-й попытки.
Поэтому, блять, запоминай, как должен выглядеть `~/.ssh/config`:
```
Host moy_ohuenniy_server
    HostName 114.154.31.54
    User ubuntu
    Port 22
    IdentitiesOnly=yes
    IdentityFile=~/.ssh/id_rsa_moy_ohuenniy_server
```
Самое главное тут - это последние 2 строки. Именно они задают ключ, который будет использован при выполнении команды `ssh moy_ohuenniy_server`. Линух не будет перебирать **ВСЕ** имеющиеся ключи, а сразу возьмет нужный, таким образом, ты не поймаешь ошибку превышения лимита попыток авторизации и не будешь потом охуевать, почему это сервер шлет тебя нахуй, если ключ правильный.
Удобно также сразу тут и задать порт. Тогда тебе не придется задавать его в других местах. Особенно важно, когда порт не стандартный 22-й. Также ты сможешь подключаться к серверу не только по его хосту (`ssh 114.154.31.54`), но и по алиасу (`ssh moy_ohuenniy_server`).
В `HostName` может быть как айпишник, так и домен.
Название в `Host moy_ohuenniy_server` может быть любым.

## keychain
Еще один крайне важный элемент. Дело в том, что если ты положил ключ в `~/.ssh/` и настроил `~/.ssh/config`, то это еще не все. Если ты попытаешься подключиться к серверу, то нихуя не выйдет. Я даже не знаю, как там на низком уровне обрабатываются ключи, вроде ssh-agent'ом. Но нас это ебать не должно, ПРОСТО ставим `keychain`:
```shell
sudo apt-get update && sudo apt-get install -y keychain
```
Далее в `~/.bashrc` добавляем в конец следуюущее:
```
###########################################################################
# allow $USER to use keys. Only enter once and it will remain enabled till
# you delete it or reboot the server
###########################################################################
/usr/bin/keychain $HOME/.ssh/id_rsa_moy_ohuenniy_server $HOME/.ssh/id_rsa_moy_vtoroy_ohuenniy_server
source $HOME/.keychain/$HOSTNAME-sh
```

Как видишь, для каждого ключа из `~/.ssh/` нужно дописать `$HOME/.ssh/id_rsa_xxx` в соответствующее keychain'овское место в `~/.bashrc`.
Спокуха, если ты думаешь, что ЭТО пердолинг, то это не пердолинг, пердолинг у тебя был бы с ssh-agent. Так что, просто добавляй одну однотипную строчку на каждый ключ в соответствующее место в `~/.bashrc` и горя знать не будешь.
`~/.bashrc` обновляется 1 раз при начале сеанса. Так что, перелогинься, чтобы применить изменения.
При новом сеансе ты должен будешь увидеть, что `keychain` подрубил ключ. Все, можно подключаться к серваку.


## Чек-лист
1. Приватный и публичный ключи лежат в `~/.ssh/` под именами `~/.ssh/id_rsa_xxx` и `~/.ssh/id_rsa_xxx.pub`. Права на них `600` и `644` соответственно.
2. В `~/.ssh/config` хосту назначен этот ключ.
3. Установлен `keychain`, ключ добавлен в `~/.bashrc`, сеанс перелогинен.

## Популярные ошибки
Q: Все настроено правильно, сервер шлет нахуй.
A: Настроен ли `~/.ssh/config`? На сервере вообще добавлен публичный ключ в `~/.ssh/authorized_keys`? Включена ли авторизация по ключам?
