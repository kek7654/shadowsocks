#!/bin/bash

ssserver \
    -U \
    --tcp-fast-open \
    --server-addr 0.0.0.0:18388 \
    --password "${PASSWORD}" \
    --encrypt-method "${METHOD}" \
    --dns "${NAMESERVER}" \
    --timeout "${TIMEOUT}" \
    --udp-timeout "${TIMEOUT}" \
    --plugin v2ray-plugin \
    --plugin-opts "server;fast-open;host=${SERVER_NAME};path=${V2RAY_PATH}"
