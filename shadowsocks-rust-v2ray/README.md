# shadowsocks-rust-v2ray

([shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust) + [v2ray](https://github.com/shadowsocks/v2ray-plugin)) behind ([nginx](https://hub.docker.com/_/nginx) + automatically [acme.sh](https://github.com/acmesh-official/acme.sh) free SSL certificate).

## Install
1. Install [Docker](https://docs.docker.com/engine/install/ubuntu/), [Docker Compose v2](https://docs.docker.com/compose/install/) and `sudo apt-get install -y git make`
2. Copy this repository: `git clone https://bitbucket.org/kek7654/shadowsocks.git && cd shadowsocks/shadowsocks-rust-v2ray && cp .env.example .env`
3. Set password and domain in the `.env` file.
4. Run project using `make start` command. (Stop using `make stop`).

Now you can have direct access to `shadowsocks+v2ray` in HTTP mode on port `1000` (`PORT_SHADOWSOCKS_NON_PROXIED`).
But the main purpose of this project is to use `shadowsocks` in TLS mode on port `443`, behind `nginx` with a trusted SSL certificate. See the next section.

## Issue a trusted SSL certificate
By default `nginx` uses a [self-signed](https://en.wikipedia.org/wiki/Self-signed_certificate) SSL certificate.

When all containers are started, you can issue and install a free trusted SSL certificate using this command:
```shell
make acme-issue-cert && make acme-install-cert
```
You need to issue and install a cert only once per domain. Then the cert will be renewed automatically.

## Proxing with Cloudflare
If your domain is already delegated to Cloudflare with `NS DNS records` and you have configured the `type A DNS record` with your server's IP in the CF panel, you need to set the SSL encryption mode to `Flexible` (default mode in CF) to issue a cert first time using the commands above.

You need to use the `Flexible` mode to issue a cert first time because `nginx` uses a self-signed cert by default and Cloudflare won't be able to connect to your (bad cert) server in `Full (strict)` mode.

Once your SSL cert has been issued, then you can set the SSL encryption mode to `Full (strict)`.

But **don't** turn on a `Always Use HTTPS` setting because `acme.sh` issues and renews certs using 80 port.

## Refactoring
- Create `README-RU.md`.
- Use a `non-root` user for shadowsocks.
- Use multiple domains and certificates simultaneously with nginx `SNI`.
- Test `IPv6`.
- Move all paths to environment variables???
- ~~OK: Precise configure of `ssl-params.conf`.~~
- ~~OK: Enable `OCSP Stapling` for nginx.~~
