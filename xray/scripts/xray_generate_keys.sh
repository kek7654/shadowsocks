#!/bin/bash

echo -e "SHADOWSOCKS PASSWORD:\n$(openssl rand -base64 32)\n"
echo -e "REALITY UUID:\n$(xray uuid)\n"
echo -e "REALITY SHORT ID:\n$(openssl rand -hex 8)\n"
echo -e "REALITY KEYS:\n$(xray x25519)"
