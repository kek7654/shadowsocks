# Xray server
При помощи данного гайда вы можете поднять сервер с двумя протоколами: `Shadowsocks-2022` и `VLESS-XTLS-uTLS-REALITY`.
А также соответствующий клиент для `Linux` (Debian-based).
Домен не требуется.

## Использование:
Все команды идентичны, что на сервере, что на клиенте. На сервере не требуется использовать `sudo`.

1. Установить зависимости:
```shell
sudo apt-get update && apt-get install -y wget unzip make rsync
```
2. Скопировать скрипты из директории `scripts` в `/usr/local/bin/` вручную или же при помощи `make deploy-scripts` / `sudo make deploy-scripts-locally`.
3. Установить `xray` и соответствующий `systemd`-сервис. `xray` устанавливается в `/usr/local/bin/`, файлы `geoip.dat` и `geosite.dat` в `/usr/local/share/xray/`, создается директория для конфигов `/usr/local/etc/xray/`, `systemd`-сервис копируется в `/etc/systemd/system/xray.service`.
```shell
sudo xray_download.sh
```
3. Создать в `/usr/local/etc/xray/` конфиг-файл. Назвать его как угодно, но не `config.json`, потому что под этим именем будет создан симлинк, указывающий на текущий используемый конфиг. Ключи можно сгенерировать при помощи `xray_generate_keys.sh`.
4. Запустить `xray`. В качестве параметра указать желаемый конфиг, только имя, без `.json`. На сервере это будет серверный конфиг, на клиенте - клиентский.
```shell
sudo xray_restart_with_config.sh my_config_name
```
5. Включить автоматический старт сервиса при загрузке:
```shell
sudo systemctl enable xray.service
```

Для повышения пропускной способности, на сервере перед запуском `xray` 1 раз запустить `xray_system_network_tuner.sh`.

В случае, если все хорошо, статус сервиса должен выглядеть как-то так:
```
● xray.service - Xray Service
     Loaded: loaded (/etc/systemd/system/xray.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2023-11-16 00:55:57 MSK; 17h ago
       Docs: https://github.com/XTLS
   Main PID: 319906 (xray)
      Tasks: 13 (limit: 18731)
     Memory: 31.9M
     CGroup: /system.slice/xray.service
             └─319906 /usr/local/bin/xray -config /usr/local/etc/xray/config.json
```

## systemctl memo:
Start the service:
```shell
systemctl daemon-reload && systemctl restart xray.service
```

Stop the service:
```shell
systemctl stop xray.service
```

Service status:
```shell
systemctl status xray.service -l --no-pager
```

To start a service automatically at boot, use the `enable` command:
```shell
systemctl enable xray.service
```

## TODO:
- OK:   `make deploy-scripts` command. The command deploys scripts into `/usr/local/bin/` of the server specified in `.env`
- OK:   `make deploy-scripts-local` command. Same as above but copies scripts locally.
- OK:   `make deploy-configs` command. Deploys configs from local machine to the remote one. All configs except the `config.json` symlink.
- OK:   optimize system tcp stack: https://shadowsocks.org/doc/advanced.html
- OK:   use `bbr`???
- OK:   create a systemd unit for client.
- OK:   set loglevel of the server to `warning` at the end.
